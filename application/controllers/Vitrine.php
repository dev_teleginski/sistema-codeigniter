<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vitrine extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->helper('url');

		$this->load->library('grocery_CRUD');
		$this->load->library('pagination');

		$this->load->model('Vitrines','',TRUE);
	}



	public function carregaTemplate($output = null, $view_location){

		$this->template->load('Template',$view_location,(array)$output);
	}


	public function index($index = 0){
		
		$resultados['dados'] = $this->Vitrines->buscarProdutos($index);
		$resultados['produtos_recentes'] = $this->Vitrines->produtosRecentes();

		$config['base_url'] = base_url().'Vitrine/index';
		$config['total_rows'] = $this->db->get('produtos')->num_rows();
		$config['per_page'] = 8;
		$config['num_links'] = 30;

		//Paginar com bootrap
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] = "</ul>";

		$config['first_tag_open'] = "<li>";
		$config['last_tag_open'] = "<li>";

		$config['next_tag_open'] = "<li>";
		$config['prev_tag_open'] = "<li>";

		$config['num_tag_open'] = "<li>";
		$config['num_tag_close'] = "</li>";
		
		$config['first_tag_close'] = "</li>";
		$config['last_tag_close'] = "</li>";

		$config['next_tag_close'] = "</li>";
		$config['prev_tag_close'] = "</li>";

		$config['cur_tag_open'] = "<li class=\"active\"><span><b>";
		$config['cur_tag_close'] = "</b></span></li>";
		//fim paginação com bootstrap

		$this->pagination->initialize($config);

		$resultados['links'] = $this->pagination->create_links();

		$this->template->load('Template','vitrine/Index', $resultados);
	}


	public function buscarDetalhes(){
		
		$parcela_id = $this->input->post('parcela_id');

		$resultado = $this->Vitrines->buscarDetalhes($parcela_id);

		if($resultado)
			echo json_encode($resultado);
		else
			echo json_encode('{"msg": "Nada encontrado"}');
	}


	public function marca(){

		$crud = new Grocery_CRUD();
		
		//set table
		$crud->set_table('marcas');

		//relação
		$crud->set_relation('pais_origem','paises','paisNome');

		//displayers
		$crud->display_as('pais_origem','Pais de origem');
		$crud->display_as('data_registro','Data de registro');

		//rules
		$crud->set_rules('nome','Nome','required');
		$crud->set_rules('data_registro','Data de registro','required|date');
		
		$this->carregaTemplate((array)$crud->render(),'marca/Index');

	}

	public function crud(){
		
		$crud = new Grocery_CRUD();

		//set table
		$crud->set_table('produtos');
		$crud->set_subject('Produtos');

		//relation
		$crud->set_relation('marca_id','marcas','nome');

		//fields
		$crud->field_type('status','dropdown',array('1' => 'Ativo', '0' => 'Inativo'));

		//upload field
		$crud->set_field_upload('img_local','assets/uploads/files');

		//unset columns
		$crud->unset_columns('img_local');

		//Regras
		$crud->set_rules('nome','Nome','required');
		$crud->set_rules('preco','Preço','required|numeric');
		$crud->set_rules('qtd_estoque','Quantidade em estoque','required|numeric');
		$crud->set_rules('data_registro','Data de registro','required|date');
		$crud->set_rules('status','Ativo ou Inativo','required');


		//Displayers
		$crud->display_as('preco','Preço');
		$crud->display_as('qtd_estoque','Quantidade em estoque');
		$crud->display_as('data_registro','Data do registro');
		$crud->display_as('status','Ativo ou Inativo');
		$crud->display_as('marca_id','Marca');
		$crud->display_as('descricao','Descrição');
		$crud->display_as('img_local','Imagem');

		$crud->callback_before_upload(array($this, 'validar_extensao'));
		
		$this->carregaTemplate((array)$crud->render(),'crud/Index');
		
	}


	public function validar_extensao($arquivo, $field){
	  
	  $file =$arquivo[$field->encrypted_field_name]['type'];

	  if ($file != 'image/png' && $file != 'image/jpeg' && $file != 'image/gif'){
	   return 'Somente as seguintes extensões estão disponíveis: png, jpg, gif. Extensão selecionada: '.$file;
	  }
	  return true;
	}

	

	/*public function offices()
	{
		$output = $this->grocery_crud->render();

		$this->_example_output($output);
	}

	public function index()
	{
		$this->_example_output((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
	}*/





	/*public function teste(){
		
		$crud = new grocery_CRUD();
		
		$crud->set_table('clientes_contatos');
		$crud->set_subject('Contatos de clientes');//seta uma lias
		
		$crud->set_relation('id_cliente','clientes','nome');//esbelece relação de duas tabelas, monstrando o nome ao inves do id do cliente, ultimo parametro é a clausula where

		$crud->columns('id_cliente','email');//adciona só que se quer ver na tabela
		$crud->fields('id_cliente','email');//adciona só que se quer ver na tabela

		$crud->display_as('id_cliente','Nome');
		


		$out = $crud->render();
		$this->_example_output($out);


	}*/

	







	/*public function offices_management()
	{
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('offices');
			$crud->set_subject('Office');
			$crud->required_fields('city');
			$crud->columns('city','country','phone','addressLine1','postalCode');

			$output = $crud->render();

			$this->_example_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function employees_management()
	{
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('employees');
			$crud->set_relation('officeCode','offices','city');
			$crud->display_as('officeCode','Office City');
			$crud->set_subject('Employee');

			$crud->required_fields('lastName');

			$crud->set_field_upload('file_url','assets/uploads/files');

			$output = $crud->render();

			$this->_example_output($output);
	}

	public function customers_management()
	{
			$crud = new grocery_CRUD();

			$crud->set_table('customers');
			$crud->columns('customerName','contactLastName','phone','city','country','salesRepEmployeeNumber','creditLimit');
			$crud->display_as('salesRepEmployeeNumber','from Employeer')
				 ->display_as('customerName','Name')
				 ->display_as('contactLastName','Last Name');
			$crud->set_subject('Customer');
			$crud->set_relation('salesRepEmployeeNumber','employees','lastName');

			$output = $crud->render();

			$this->_example_output($output);
	}

	public function orders_management()
	{
			$crud = new grocery_CRUD();

			$crud->set_relation('customerNumber','customers','{contactLastName} {contactFirstName}');
			$crud->display_as('customerNumber','Customer');
			$crud->set_table('orders');
			$crud->set_subject('Order');
			$crud->unset_add();
			$crud->unset_delete();

			$output = $crud->render();

			$this->_example_output($output);
	}

	public function products_management()
	{
			$crud = new grocery_CRUD();

			$crud->set_table('products');
			$crud->set_subject('Product');
			$crud->unset_columns('productDescription');
			$crud->callback_column('buyPrice',array($this,'valueToEuro'));

			$output = $crud->render();

			$this->_example_output($output);
	}

	public function valueToEuro($value, $row)
	{
		return $value.' &euro;';
	}

	public function film_management()
	{
		$crud = new grocery_CRUD();

		$crud->set_table('film');
		$crud->set_relation_n_n('actors', 'film_actor', 'actor', 'film_id', 'actor_id', 'fullname','priority');
		$crud->set_relation_n_n('category', 'film_category', 'category', 'film_id', 'category_id', 'name');
		$crud->unset_columns('special_features','description','actors');

		$crud->fields('title', 'description', 'actors' ,  'category' ,'release_year', 'rental_duration', 'rental_rate', 'length', 'replacement_cost', 'rating', 'special_features');

		$output = $crud->render();

		$this->_example_output($output);
	}

	public function film_management_twitter_bootstrap()
	{
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('twitter-bootstrap');
			$crud->set_table('film');
			$crud->set_relation_n_n('actors', 'film_actor', 'actor', 'film_id', 'actor_id', 'fullname','priority');
			$crud->set_relation_n_n('category', 'film_category', 'category', 'film_id', 'category_id', 'name');
			$crud->unset_columns('special_features','description','actors');

			$crud->fields('title', 'description', 'actors' ,  'category' ,'release_year', 'rental_duration', 'rental_rate', 'length', 'replacement_cost', 'rating', 'special_features');

			$output = $crud->render();
			$this->_example_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	function multigrids()
	{
		$this->config->load('grocery_crud');
		$this->config->set_item('grocery_crud_dialog_forms',true);
		$this->config->set_item('grocery_crud_default_per_page',10);

		$output1 = $this->offices_management2();

		$output2 = $this->employees_management2();

		$output3 = $this->customers_management2();

		$js_files = $output1->js_files + $output2->js_files + $output3->js_files;
		$css_files = $output1->css_files + $output2->css_files + $output3->css_files;
		$output = "<h1>List 1</h1>".$output1->output."<h1>List 2</h1>".$output2->output."<h1>List 3</h1>".$output3->output;

		$this->_example_output((object)array(
				'js_files' => $js_files,
				'css_files' => $css_files,
				'output'	=> $output
		));
	}

	public function offices_management2()
	{
		$crud = new grocery_CRUD();
		$crud->set_table('offices');
		$crud->set_subject('Office');

		$crud->set_crud_url_path(site_url(strtolower(__CLASS__."/".__FUNCTION__)),site_url(strtolower(__CLASS__."/multigrids")));

		$output = $crud->render();

		if($crud->getState() != 'list') {
			$this->_example_output($output);
		} else {
			return $output;
		}
	}

	public function employees_management2()
	{
		$crud = new grocery_CRUD();

		$crud->set_theme('datatables');
		$crud->set_table('employees');
		$crud->set_relation('officeCode','offices','city');
		$crud->display_as('officeCode','Office City');
		$crud->set_subject('Employee');

		$crud->required_fields('lastName');

		$crud->set_field_upload('file_url','assets/uploads/files');

		$crud->set_crud_url_path(site_url(strtolower(__CLASS__."/".__FUNCTION__)),site_url(strtolower(__CLASS__."/multigrids")));

		$output = $crud->render();

		if($crud->getState() != 'list') {
			$this->_example_output($output);
		} else {
			return $output;
		}
	}

	public function customers_management2()
	{
		$crud = new grocery_CRUD();

		$crud->set_table('customers');
		$crud->columns('customerName','contactLastName','phone','city','country','salesRepEmployeeNumber','creditLimit');
		$crud->display_as('salesRepEmployeeNumber','from Employeer')
			 ->display_as('customerName','Name')
			 ->display_as('contactLastName','Last Name');
		$crud->set_subject('Customer');
		$crud->set_relation('salesRepEmployeeNumber','employees','lastName');

		$crud->set_crud_url_path(site_url(strtolower(__CLASS__."/".__FUNCTION__)),site_url(strtolower(__CLASS__."/multigrids")));

		$output = $crud->render();

		if($crud->getState() != 'list') {
			$this->_example_output($output);
		} else {
			return $output;
		}
	}*/

}
