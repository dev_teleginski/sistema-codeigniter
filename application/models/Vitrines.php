<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Vitrines extends CI_Model{
	
	function __construct(){
		parent::__construct();
	}


	public function buscarProdutos($index){

		$campos = 'pro.nome, pro.descricao, pro.img_local,pro.preco,pro.id_produto,';
		$campos .= 'm.nome as marca_nome';

		$this->db->select($campos);
		$this->db->join('marcas m','id_marca = pro.marca_id');
		$this->db->join('paises p','p.paisId = m.pais_origem');
		$this->db->where('pro.status = 1');
		$this->db->where('pro.qtd_estoque > 0');
		$this->db->order_by('pro.id_produto','DESC');

		$query = $this->db->get('produtos pro', '8', $index);

		return ($query->num_rows() > 0)?$query->result() : FALSE;
	}

	public function produtosRecentes(){
		$campos = 'pro.nome, pro.descricao, pro.img_local,pro.preco,pro.id_produto,';
		$campos .= 'm.nome as marca_nome';

		$this->db->select($campos);
		$this->db->join('marcas m','id_marca = pro.marca_id');
		$this->db->join('paises p','p.paisId = m.pais_origem');
		$this->db->where('pro.status = 1');
		$this->db->where('pro.qtd_estoque > 0');
		$this->db->order_by('pro.id_produto','DESC');
		$query = $this->db->get('produtos pro','4');

		return ($query->num_rows() > 0)?$query->result() : FALSE;
	}

	public function buscarDetalhes($id_produto){

		$campos = 'm.nome as marca_nome, p.paisNome, pro.*';

		$this->db->select($campos);
		$this->db->join('marcas m','id_marca = pro.marca_id');
		$this->db->join('paises p','p.paisId = m.pais_origem');
		$this->db->where('pro.id_produto',$id_produto);
		$query = $this->db->get('produtos pro');

		return ($query->num_rows() > 0)?$query->result() : FALSE;	
	}

}
?>