<?php
	//funcão para formatação de numero
	function format_valor($n){
		return 'R$ '.number_format($n, 2, ',', '.');
	}
?>

<style type="text/css">
	body{
		background-image: url('<?php echo base_url()?>assets/img/circles-light.png');
	}
	

	.panel-default:hover{
		border: 1px solid #BFBFBF;
    	background-color: white;
    	box-shadow: 10px 5px 5px #ddd;
	}
	
	.panel-default>.panel-heading {
	    background-image: -webkit-linear-gradient(top,#ff7f51 0,#ff7f51 100%);
	    background-image: -o-linear-gradient(top,#ff7f51 0,#ff7f51 100%);
	    background-image: -webkit-gradient(linear,left top,left bottom,from(#ff7f51),to(#e8e8e8));
	        background-image: linear-gradient(to bottom,#ff7f51 0,#ff7f51 100%);
	    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fff5f5f5', endColorstr='#ffe8e8e8', GradientType=0);
	    background-repeat: repeat-x;
	    color:white;
	}
	
	.panel-default>.panel-heading .badge {
    	color: #f5f5f5;
    	background-color: #4f000b;
	}

	.modal-header {
	    padding: 15px;
	    border-bottom: 1px solid #ff7f51;
	    background-color: #ff7f51;
	    color:white;
	}

	.carousel-control.left {
	    background-image: -webkit-linear-gradient(left,rgba(0,0,0,.0) 0,rgba(0,0,0,0) 100%);
	    background-image: -o-linear-gradient(left,rgba(0,0,0,.0) 0,rgba(0,0,0,.0001) 100%);
	    background-image: -webkit-gradient(linear,left top,right top,from(rgba(0,0,0,.5)),to(rgba(0,0,0,.0001)));
	    background-image: linear-gradient(to right,rgba(0,0,0,.0) 0,rgba(0,0,0,.0001) 100%);
	    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#80000000', endColorstr='#00000000', GradientType=1);
	    background-repeat: repeat-x;
	}

	.carousel-control.right {
	    background-image: -webkit-linear-gradient(left,rgba(0,0,0,.0) 0,rgba(0,0,0,0) 100%);
	    background-image: -o-linear-gradient(left,rgba(0,0,0,.0) 0,rgba(0,0,0,.0001) 100%);
	    background-image: -webkit-gradient(linear,left top,right top,from(rgba(0,0,0,.5)),to(rgba(0,0,0,.0001)));
	    background-image: linear-gradient(to right,rgba(0,0,0,.0) 0,rgba(0,0,0,.0001) 100%);
	    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#80000000', endColorstr='#00000000', GradientType=1);
	    background-repeat: repeat-x;
	}

	.carousel-caption {
  		
  		background-image: -webkit-linear-gradient(left,rgba(79,0,11,.8) 0,rgba(79,0,11,.8) 100%);
  		padding: 10px

	}
	#carousel-example-generic{
		margin-bottom: 10px;
		background-color: white;
    	box-shadow: 10px 5px 5px #ddd;
	}

	.paginacao{
		text-align: center;
	}

	.pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover {
		    z-index: 3;
		    color: #fff;
		    cursor: default;
		    background-color: #ff7f51;
		    border-color: #ff7f51;
		}


	/*FOOTER*/
	.footer{
		background-color: #ff7f51;
		padding: 20px;
		color:white;
		text-align: left;
	}

	.nav-pills>li.active>a, .nav-pills>li.active>a:focus, .nav-pills>li.active>a:hover {
    	color: #fff;
    	background-color: #4f000b;
    	border-radius: 0px
	}

	.nav-stacked li{
		background-color: white;
		margin-bottom: 5px;
	}

	.nav-stacked a{
		color:#4f000b;
	}

	.icon_size{
		font-size: 70px;
	}

	.fa-facebook-f:hover{
		color:#3b5998;
	}

	.fa-whatsapp:hover{
		color:#25d366;
	}

	.fa-whatsapp{
		margin-right: 20px;
		margin-left: 20px;

	}

	.fa-instagram:hover{
		color:#cd486b;
	}

	.icones{
		text-align: center;
	}
	.icones a{
		color:white;
	}

	@media only screen and (max-width: 600px) {
		.carousel-caption h2,p{
  			font-size:12px;
		}
	}
</style>


<!-- SLIDE-->
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    
    <div class="active item">
      <img src="<?php echo base_url()?>assets/img/slide.png" alt="...">
      
      <div class="carousel-caption">
        <h1>FoxCloth - Roupas e acessórios</h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
      </div>
    </div>

    
  </div>
</div>
<!-- FIM SLIDE-->


<?php if(isset($dados) && !empty($dados)):?>
		
	   <?php
	   $chunk = array_chunk($dados, 4);
	   foreach ($chunk as $dados):?>
	   		<div class="row"><!-- INICIO ROW-->
				<?php foreach ($dados as $d):?>

					<div class="col-md-3 painel">
						<div class="panel panel-default">
						  <div class="panel-heading"><b><?php echo $d->nome.' '.$d->marca_nome ?></b> <span class="badge badge-default pull-right" title="Valor"><?php echo format_valor($d->preco) ?></span></div>
						  <div class="panel-body" align="justify">

						  		<?php if($d->img_local):?>
						  			<img src="<?php echo base_url()?>assets/uploads/files/<?php echo $d->img_local ?>" class="img-responsive" alt="<?php  echo $d->nome.' '.$d->marca_nome ?>" title="<?php  echo $d->nome ?>">
						  			<hr>
						  		<?php endif; ?>


						  		<p><?php echo substr($d->descricao, 0, 550).'...'; ?></p>
						  		
						  </div>
						  <a class="btn btn-primary btn-block btn-xs" title="Detalhes" id="detalhe-<?php echo $d->id_produto?>" onclick="abrir_detalhe(this)">Detalhes</a>
						</div>
					</div>

				<?php endforeach; ?>
			</div><!-- FIM ROW-->

		<?php endforeach; ?>

<?php endif; ?>

<div class="paginacao">
	<?php echo $links ?>
</div>

</div>




<style type="text/css">
	
</style>
<!-- RodaPé-->
<footer class="footer">
	<div class="row">
		<div class="col-md-12">
			
			<!--Informações da empresa -->
			<div class="col-md-4">
				<h1 align="center">Contatos</h1>
				<h4>O trecho padrão original de Lorem Ipsum, usado desde o século XVI, está reproduzido abaixo para os interessados.</h4>
				<ul>
					<li><h4 title="Endereço">Rua alguma coisa Curitiba/PR - 81170210</h4></li>
					<li><h4 title="E-mail">algumacoisa@foxcloth.com.br</h4></li>
					<li><h4 title="Telefone">(41) 3333-4444</h4></li>
				</ul>

				<div class="icones">
					<a href="https://facebook.com" target="_blank"><i class="fab fa-facebook-f icon_size"></i></a>
					<a href="tel:41999999999" target="_blank"><i class="fab fa-whatsapp icon_size"></i></a>
					<a href="https://www.instagram.com" target="_blank"><i class="fab fa-instagram icon_size"></i></a>
				</div>
			</div>

			<!-- Produtos recentes -->
			<div class="col-md-4">
				<h1 align="center">Produtos recentes</h1>
				<ul class="nav nav-pills nav-stacked">
				  
				  <?php 
				 	if(isset($produtos_recentes) && !empty($produtos_recentes)){ 
					  	$contador = 0;
					  	foreach ($produtos_recentes as $dado):
					  		if($contador == 4)break;?>

					  		<li role="presentation"><a class="btn btn-defalt" onclick="abrir_detalhe(this)" id="detalhe-<?php echo $dado->id_produto ?>"><?php echo $dado->nome.' '.$dado->marca_nome; ?></a></li>
				  	
				  		<?php $contador ++;endforeach; 
					}else{
						echo '<p align="center">Nada encontrado!</p>';
					}?>
				
				</ul>
			</div>

			<!-- LINKS-->
			<div class="col-md-4">
				<h1>Links</h1>
				<ul class="nav nav-pills nav-stacked footer-links">
				  <li role="presentation" class="active"><a rel="canonical" href="<?php echo base_url();?>/crud">Vitrine</a></li>
				  <li role="presentation"><a rel="canonical" href="<?php echo base_url();?>Vitrine/crud">Admin</a></li>
				</ul>
			</div>

		</div>
	</div>

</footer>

<section style="background-color: #4f000b;color:white;">
			<p align="center" style="margin: 0 0 0px">@<?php echo date('Y')?> FoxCloth</p>
</section>





<!-- Modal -->
<div id="modal_detalhe" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="modal_titulo"></h4>
      </div>
      <div class="modal-body">
        	<div id="conteudo_modal"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
      </div>
    </div>

  </div>
</div>
<!-- FIM Modal -->

<script type="text/javascript">
	$("#menu_vitrine").addClass("active");//adiciona a class active ao menu


	function abrir_detalhe(input){
		var tipo = input.id.split('-');
		var parcela_id = tipo[1];

		$.ajax({

            url : '<?php echo base_url()?>Vitrine/buscarDetalhes',  
            type : 'POST',
            dataType: 'json',
            data : {
               parcela_id:parcela_id
          	},
            
            beforeSend : function(){
				//alert("Enviando..");
          	
          	},success: function(data){
   				console.log(data);

   				//set titulo modal
   				$("#modal_titulo").text(data[0].id_produto+' '+data[0].nome+' '+data[0].marca_nome);

   				var html ='';

   				//CONTEUDO DO MODAL
				html += '<div class="row">';
	   				html += '<div class="col-md-12">';
	   					
	   					html += "<h4 align='center'>Detalhes do produto<h4><hr>"

		   				html += '<div class="col-md-6">';
		   					html += '<img src="<?php echo base_url()?>assets/uploads/files/'+data[0].img_local+'" class="img-responsive">';
		   				html += '</div>';

		   				html += '<div class="col-md-6" align="justify">';
		   					html += '<h5><b>Nome do Produto:</b> '+data[0].nome+'</h5>';
		   					html += '<h5><b>Marca:</b> '+data[0].marca_nome+'</h5>';
		   					html += '<h5><b>Preço:</b> '+data[0].preco.replace('.',',')+'</h5>';
		   					html += '<h5><b>Pais de fabricação:</b> '+data[0].paisNome+'</h5>';
		   					html += '<h5><b>Quantidade em estoque:</b> '+data[0].qtd_estoque+'</h5>';
		   					html += '<h5><b>Data de publicação:</b> '+dataAtualFormatada(data[0].data_registro)+'</h5>';
		   					html += '<hr><h5><b>Descrição:</b> <p>'+data[0].descricao+'</p></h5>';
		   				html += '</div>';

	   				html += '</div>';
   				html += '</div>';
   				//FIM DO CONTEUDO DO MODAL

   				

   				$("#conteudo_modal").html(html);
				$('#modal_detalhe').modal();

            },error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }

        });

	}


	function dataAtualFormatada(){
	    var data = new Date();
	    var dia = data.getDate();
	    if (dia.toString().length == 1)
	      dia = "0"+dia;
	    var mes = data.getMonth()+1;
	    if (mes.toString().length == 1)
	      mes = "0"+mes;
	    var ano = data.getFullYear();  
	    return dia+"/"+mes+"/"+ano;
	}

</script>




