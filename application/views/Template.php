<!DOCTYPE html>
<html>
<head>
	
  
  <meta property="og:title" content="Loja de roupas e acessórios">
  <meta property="og:description" content="A foxcloth é uma empresa focada na venda de roupas e acessórios. Clique e nos conheça melhor!">
  <meta property="og:url" content="<?php echo base_url()?>Vitrine/index">
  <meta property="og:site_name" content="Foxcloth">
  <meta property='og:image' content='<?php echo base_url()?>assets/img/fox_logo.png'/>
  <meta property="og:type" content="website">

  <title>Foxcloth - Loja de roupas</title>


	<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>

<link href="https://fonts.googleapis.com/css?family=Montserrat|Roboto+Condensed" rel="stylesheet">
<link rel="icon" href="<?php echo base_url()?>assets/img/fox.ico">

<script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>



</head>
<body>

<style type="text/css">
	.footer {
		
		left: 0;
		bottom: 0;
		width: 100%;
		background-color: #e7e7e7;
		color: black;
		text-align: center;
	}

  .navbar-nav{
    margin: 10px;
  }

  .navbar-default .navbar-nav>.active>a, .navbar-default .navbar-nav>.open>a {
    background-image: -webkit-linear-gradient(top,#ff7f51 0,#ff7f51 100%);
    background-image: -o-linear-gradient(top,#ff7f51 0,#ff7f51 100%);
    color:white;
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffdbdbdb', endColorstr='#ffe2e2e2', GradientType=0);
    
  }

  body{
    margin-top: 100px;
    font-family: 'Montserrat', sans-serif;
    font-family: 'Roboto Condensed', sans-serif;
  }

  .btn {
    padding: 5px 24px;
    border: 0 none;
    border-radius: 0px;
    font-weight: 700;
    letter-spacing: 1px;
    text-transform: uppercase;
}
 
  .btn:focus, .btn:active:focus, .btn.active:focus {
      outline: 0 none;
  }
   
  .btn-primary {
      background: #ff7f51;
      color: #ffffff;
  }
   
  .btn-primary:hover, .btn-primary:focus, .btn-primary:active, .btn-primary.active, .open > .dropdown-toggle.btn-primary {
      background: #4f000b;
  }
   
  .btn-primary:active, .btn-primary.active {
      background: #007299;
      box-shadow: none;
  }
</style>

<!-- NAVBAR MENU-->
<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <img src="<?php echo base_url() ?>assets/img/fox_logo.png" class="img-responsive" style="width: 90px;margin-top: 5px;margin-bottom: 5px">
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        
        <li id="menu_crud"><a rel="canonical" href="<?php echo base_url()?>Vitrine/crud">Admin</a></li>
        <li id="menu_vitrine"><a rel="canonical" href="<?php echo base_url()?>Vitrine/index">Vitrine</a></li>
        
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>


<!-- FIM NAVBAR-->


<div class="container">
	<?php echo $contents ?>
</div>





<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</body>
</html>