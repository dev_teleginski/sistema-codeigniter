<?php 
foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>

<style type="text/css">
	#sub_menu{
		margin-bottom: -15px
	}
</style>

<!-- submenus -->
<div id="sub_menu">
	<ul class="nav nav-tabs">
	  <li><a href="<?php echo base_url()?>Vitrine/crud/">Produtos</a></li>
	  <li class="active"><a href="<?php echo base_url()?>Vitrine/marca/">Marcas</a></li>
	</ul>

	<ul class="breadcrumb">
		<li><a style="color:green" href="<?php echo base_url()?>Vitrine/crud/">CRUD</a></li>
	</ul>
</div>
<!-- submenus -->

<div class="col-md-12">

	<!--inicio row-->
	<div class="row">

		<?php echo $output; ?>
		
	</div><!--Fim row-->
</div>

<?php foreach($js_files as $file): ?>
    <script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>


<script type="text/javascript">

	$("#menu_crud").addClass("active");
</script>

