##Observações iniciais
No projeto presente foi acrescentado uma biblioteca chamada Template.php, para que se possa utilizar a mesma base estrutural HTML em todas as páginas, alterando apenas o conteúdo.
Na raiz do projeto se encontra o arquivo "sistema.sql" ao qual possui todas as tabelas necessárias para o projeto.

##Permissões
Apenas a pasta "/assets/uploads/files" precisa de permissão de leitura e escrita.

##Efetuar CRUD
Ao entrar no caminho /Vitrine/crud e no submenu "Produtos" você terá acesso ao CRUD de produtos, e caso não exista uma marca cadastrada, será necessário cadastrá-la clicando no submenu "Marcas".
Apenas os campos Descrição, imagem e marca não são obrigatórios ao preencher o formulário do produto.

##Após o cadastrado do produto
Para visualização dos produtos use o caminho /Vitrine/index
